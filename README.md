## Synopsis

This code is used to deploy an Application Stack on an Ubuntu 18.04 VM to set up a VM   
It should be run from the host on which you want it deployed - this is where you either
call and Ansible playbook or run a bash shell script


## Usage

```
./install.sh
```

